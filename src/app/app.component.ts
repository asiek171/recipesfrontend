import { Component } from '@angular/core';
import { Recipe } from './interfaces/Recipe.model';
import { HttpService } from './services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'client-app';

  public recipes: Recipe[] = [];

  constructor(private httpService: HttpService){} 
  public getAll = () => {
    let route: string = 'https://localhost:44331/api/recipe/1';
    this.httpService.getData(route)
      .subscribe((result) => {
        this.recipes = result as Recipe[];
      },
        (error) => {
          console.error(error);
        });
  }
}
