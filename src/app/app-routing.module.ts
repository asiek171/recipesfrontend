import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateComponent } from './create/create.component'
import { GetallComponent } from './getall/getall.component'
import { GetbyidComponent } from './getbyid/getbyid.component'

const routes: Routes = [
  { path: 'create', component: CreateComponent},
  { path: 'getall', component: GetallComponent},
  { path: 'getbyid/:id', component: GetbyidComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
