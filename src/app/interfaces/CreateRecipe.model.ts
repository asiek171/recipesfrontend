export interface CreateRecipe {
        Title  :string,
             Difficulty  :number,
            MakingTime  :string,
             Portions  :number,
            Description  :string,
            Ingrediens  :string,
             CategoryId  :number,
             AuthorId  :number
}