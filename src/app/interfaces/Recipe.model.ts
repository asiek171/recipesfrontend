import { Author } from "./Author.model";
import { Categories } from "./Categories.model";

export interface Recipe {
     Id: string,
     Title: string,
     Difficulty: Difficulty,
     MakingTime: string,
     Portions: number,
     Description: string,
     Ingrediens: string,
     CategoryId: number,
     Category: Categories,
     AuthorId: number,
     Author: Author
}

enum Difficulty {
     Easy = 1,
     Medium = 2,
     Hard = 3,
   }